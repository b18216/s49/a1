
fetch('https://jsonplaceholder.typicode.com/posts')
.then(response => response.json())
.then(data=>showPost(data))

const showPost = (posts) =>{
    let postEntries = '';
    posts.forEach(post=>{
        console.log()
        postEntries +=`
        <div id="post-${post.id}">
            <h3 id="post-title-${post.id}">${post.title}</h3>
            <p id="post-body-${post.id}">${post.body}</p>
            <button onclick="editPost('${post.id}')">Edit</button>
            <button onclick="deletePost ('${post.id}')">Delete</button>
        </div>
        `
    })
    document.querySelector('#div-post-entries').innerHTML = postEntries;
}
// POST
document.querySelector('#form-add-post').addEventListener('submit', (e)=>{
    
    e.preventDefault();

    fetch('https://jsonplaceholder.typicode.com/posts',
    {
        method:'POST',
        body:JSON.stringify({
            title: document.querySelector("#txt-title").value,
			body: document.querySelector("#txt-body").value,
			userId: 1

        }),
        headers:{
            "Content-Type":"application/json"
        }
    })
    .then(response=> response.json())
    .then(data =>{
        console.log(data)
        alert('Successfull')


    })
})



// EDIT

const editPost = (id)=>{
    let title = document.querySelector(`#post-title-${id}`).innerHTML;
    let body = document.querySelector(`#post-body-${id}`).innerHTML;

    document.querySelector('#txt-edit-id').value = id;
    document.querySelector('#txt-edit-title').value = title;
    document.querySelector('#txt-edit-body').value = body;

    document.querySelector('#btn-submit-update').removeAttribute('disabled');

}

// update post

document.querySelector('#form-edit-post').addEventListener('submit', (e)=>{
    e.preventDefault()

    fetch('https://jsonplaceholder.typicode.com/posts/1', {

    method:'PUT',
    body: JSON.stringify({
        id:document.querySelector('#txt-edit-id').value,
        title: document.querySelector('#txt-edit-title').value,
        body: document.querySelector('#txt-edit-body').value,
        userId: 1
    }),
    headers:{
        'Content-type': 'application/json'
    }
    })
    .then(response =>response.json())
    .then(data=>{
        console.log(data)
        alert('post updated')
    

    document.querySelector('#txt-edit-id').value = '';
    document.querySelector('#txt-title-id').value = '';
    document.querySelector('#txt-body-id').value = '';

    document.querySelector('#btn-submit-update').setAttribute('disabled', true);
})
})

// DELETE

const deletePost = (id)=>{

    fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {

        method:'DELETE'
       
        })
        .then(response =>response.json())
        .then(data=>{
            console.log(data)
            alert('Item Deleted')
    
})
document.querySelector(`#post-${id}`).remove()

}